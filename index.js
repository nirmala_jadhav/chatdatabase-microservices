const express = require('express')
const app = express()
var port = process.env.PORT || 8080
app.use(express.json({extended: false}))

const cors = require('cors')//New for microservice
app.use(cors())//New for microservice

app.listen(port, () => 
    console.log(`HTTP Server with Express.js is listening on port:${port}`))

//MongoDB connection
const MongoClient = require('mongodb').MongoClient;
const bcrypt = require('bcrypt');
const saltRounds = 10;
const mongourl = "mongodb+srv://cca-team9:ccateam92021@ccajadhavn1vaddis1.og1f7.mongodb.net/cca-project?retryWrites=true&w=majority";
const dbClient = new MongoClient(mongourl, {useNewUrlParser: true, useUnifiedTopology: true});
dbClient.connect(err => {
    if (err) throw err;
    console.log("Connected to the MongoDB cluster");
});

app.get('/', (req,res) => {
    res.send("User Account microservices by Nirmala Jadhav And Sai Surya Vaddis");
    // res.sendFile(__dirname + "/signup.html");
})

// Create Account 
app.post('/signup', (req,res) => {
    const {username, password, email, fullname} = req.body;
    console.log("Username", username);
    console.log("Password", password, fullname, email);

    // Validate for empty fields
    if (username.length<=0 || password.length<=0 || email.length<=0 || fullname.length<=0) 
        res.send({statusMsg:"Please enter all the details and Try again!", status:false});

    const db = dbClient.db();

    // Check if username already taken
    db.collection("users").findOne({username:username}, (err,user) => {
        if(user) { // If yes, then return with error 
            console.log("User Found", user);
            return res.send({statusMsg:"Username Alrpeady Exists!", status:false});
        } else { // Save the user
            console.log("Adding User", username);

            // Hash the password before saving
            bcrypt.hash(password, saltRounds, function(err, hash) {
                if(err) {
                    return res.send({statusMsg:"Signup unsuccessful!", status:false});
                }
                let newUser = {
                    username: username,
                    password: hash,
                    email: email,
                    fullname: fullname
                }
                db.collection("users").insertOne(newUser, (err, result) => {
                    if(err) {
                        return res.send({statusMsg:"Signup unsuccessful", status:false});
                    }
                    return res.send({statusMsg:"Signup successful", status:true});
                })
            });
        }
    });
});

app.post('/login', (req,res) => {
    const {username, password} = req.body;
    console.log("Username", username);
    console.log("Password", password);

    // Validate for empty fields
    if (username.length<=0 || password.length<=0) 
        return res.send({statusMsg:"Username/Password empty", status:false});

    const db = dbClient.db();

    db.collection("users").findOne({username:username}, (err,user) => {
        if(user) { // Check if valid username
            console.log("User details", user, password);

            // Compare password
            bcrypt.compare(password, user.password, function(err, result) {
                if(result) {
                    console.log("Login suceess", result, err);
                    details = {
                        username : user.username,
                        fullname : user.fullname
                    }
                    return res.send({statusMsg:"Success", status:true, details});
                }

                console.log("Login failed");
                return res.send({statusMsg:"Login Failed", status:false});
                
            });
        } else {
            console.log("User Not Found", username);
            return res.send({statusMsg:"Username Not Exists!", status:false});
        }
    });

});
